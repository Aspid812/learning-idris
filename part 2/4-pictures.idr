
import Data.Vect

-- Use `:doc Shape` to view information, provided via |||

||| Represents shapes
data Shape = ||| A triangle, with its base length and height 
             Triangle Double Double
             | ||| A rectangle, with its length and height
             Rectangle Double Double -- width and height
             | ||| A circle, with its radius
             Circle Double -- radius

-- Implement interactively function `area` which calculates area of the given shape

area : Shape -> Double
area (Triangle base height)   = 0.5 * base * height
area (Rectangle width height) = width * height
area (Circle radius)          = pi * radius * radius

-- Add documenting comments to this datatype representing pictures

||| A picture is a combination of several primitive `Shape`s in varying positions
data Picture : Type where
  ||| Just `Shape`, nothing excess
  Primitive : Shape -> Picture
  ||| A union of two `Picture`s is a `Picture` too
  Combine : Picture -> Picture -> Picture
  ||| Rotate the underlying `Picture` for the specified `angle`
  Rotate : (angle : Double) -> Picture -> Picture
  ||| Translate the underlying `Picture` for the specified vector `(dx, dy)`
  Translate : (dx : Double) -> (dy : Double) -> Picture -> Picture

-- Make sure you understand the meaning of word "translate" in this context

-- Examples of pictures
rectangle : Picture
rectangle = Primitive (Rectangle 20 10)

circle : Picture
circle = Primitive (Circle 5)

triangle : Picture
triangle = Primitive (Triangle 10 10)

test_picture : Picture
test_picture = Combine (Translate 5 5 rectangle)
                    (Combine (Translate 35 5 circle)
                    (Translate 15 25 triangle))

||| `Picture` is almost `Foldable`, except it stores only `Shape`s, not an arbitrary data.
||| Well, just fix it quickly with GADTs and scotch tape!
data PictureT : Type -> Type where    -- What does it means, 'T'? I don't know, maybe you?
  FromPicture : Picture -> PictureT Shape

implementation Foldable PictureT where
  -- foldr : (elem -> acc -> acc) -> acc -> PictureT elem -> acc
  foldr f a (FromPicture pic) = foldr' pic a where
    foldr' : Picture -> acc -> acc
    foldr' (Primitive shape)    = f shape
    foldr' (Combine pic1 pic2)  = foldr' pic1 . foldr' pic2
    foldr' (Rotate _ pic1)      = foldr' pic1
    foldr' (Translate _ _ pic1) = foldr' pic1

  -- foldl : (acc -> elem -> acc) -> acc -> PictureT elem -> acc
  foldl f a (FromPicture pic) = foldl' a pic where
    foldl' : acc -> Picture -> acc
    foldl' a (Primitive shape)    = f a shape
    foldl' a (Combine pic1 pic2)  = foldl' (foldl' a pic1) pic2
    foldl' a (Rotate _ pic1)      = foldl' a pic1
    foldl' a (Translate _ _ pic1) = foldl' a pic1


-- Implement interactively
picture_area : Picture -> Double
-- N.B. Overlapping shapes cause a surprising result, but it's a feature
picture_area = foldr ((+) . area) 0.0 . FromPicture


||| Turn a semigroup `(a, op)` into monoid `(Maybe a, monoidal op)`
||| by attaching an identity element `Nothing`. `Just` encodes elements
||| of the original semigroup.
monoidal : (a -> a -> a) -> Maybe a -> Maybe a -> Maybe a
monoidal op mx       Nothing  = mx
monoidal op Nothing  my       = my
monoidal op (Just x) (Just y) = Just (x `op` y)

||| Returns the area of the biggest triangle in a picture
biggestTriangle : Picture -> Maybe Double
biggestTriangle = foldr (monoidal max) Nothing . map (Just . area) . filter isTriangle . toList . FromPicture where
  isTriangle : Shape -> Bool
  isTriangle (Triangle _ _) = True
  isTriangle  _             = False
