||| Calculates length of the given list
length' : List a -> Nat
length' = foldr (const S) Z

||| Returnes reversed list
reverse' : List a -> List a
reverse' = foldl (flip (::)) Nil

||| Calculates sum of the fist and the last elements of the given list 
||| or returns 0 if the list is empty
sum_fl : List Integer -> Integer
sum_fl nums = fromMaybe 0 $ [| head' nums + last' nums |]

||| Returns length of the given list if there exists
||| 0 in there or returns (-1) otherwise
len_withZero : List Integer -> Int
-- len_withZero xs = maybe (-1) toIntNat $ elemIndex 0 xs *> pure (length xs)
-- Use `filter` and `length`.
-- Okay... you're the boss.
len_withZero xs = case filter (0 ==) xs of
                    Nil => -1
                    _   => toIntNat (length xs)


{-
 Define datatype with four possible values corresponding to various 
 list calculations:
   * sum
   * product
   * maximum
   * minimum
   
 Define function, which processes list according to the specified 
 calculation. If list is empty result should be Nothing.
-}

data Calculation : Type where
  Sum     : Calculation
  Product : Calculation
  Maximum : Calculation
  Minimum : Calculation

||| Turn a semigroup `(a, op)` into monoid `(Maybe a, monoidal op)`
||| by attaching an identity element `Nothing`. `Just` encodes elements
||| of the original semigroup.
monoidal : (a -> a -> a) -> Maybe a -> Maybe a -> Maybe a
monoidal op mx       Nothing  = mx
monoidal op Nothing  my       = my
monoidal op (Just x) (Just y) = Just (x `op` y)

process_list : Calculation -> (l: List Integer) -> Maybe Integer
-- There is a nicer version: `concatMap Just`, but for a some puzzling
-- cause I have failed to force Idris to use `collectJust` semigroup and
-- the corresponding monoid. Despite of all my strugglings, it stubbornly
-- keeps on using prioretized `leftmost` semigroup/monoid and dropping
-- the whole list except its head.
process_list calc = foldr (monoidal op) Nothing . map pure where
  op = case calc of { Sum => (+);  Product => (*);  Maximum => max;  Minimum => min }


{-
 Use previously defined function and datatype in the case when input data 
 are given in a string (first word in the string can be either SUM, PROD, 
 MIN, or MAX). Define whatever auxiliary functions you need.
 Your should return Nothing if the given string is somehow incorrect.
 
 Hint: functions `map` and `cast` may be useful.
 
 Recommendation: don't try to overcomplicate this function checking what 
 exactly follows the first word, just cast it to an Integer without doubt.
-}

decap : List a -> Maybe (a, List a)
decap Nil     = Nothing
decap (a::as) = Just (a, as)

process_string : String -> Maybe Integer
process_string str = do
  (cmd, params) <- decap (words str)
  calc <- lookup cmd [("SUM", Sum), ("PROD", Product), ("MAX", Maximum), ("MIN", Minimum)]
  process_list calc (map cast params)


-- Should be True
test : Bool
test = (process_string "SUM" == Nothing)
       && (process_string "SUM 1 2 3" == Just 6)
       && (process_string "MAX 5 2 3" == Just 5)
       && (process_string "PROD 1 0 5" == Just 0)
       && (process_string "MIN 1 0 5" == Just 0)
