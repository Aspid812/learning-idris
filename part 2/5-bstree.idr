data BSTree : (a: Type) -> Type where
     Empty : Ord a => BSTree a
     Node : Ord a => (left : BSTree a) -> (val : a) ->
                        (right : BSTree a) -> BSTree a

%name BSTree t, t1
                        
insert : a -> BSTree a -> BSTree a
insert x Empty = Node Empty x Empty
insert x orig@(Node left val right) = 
     case compare x val of
        LT => Node (insert x left) val right
        EQ => orig
        GT => Node left val (insert x right)


implementation Foldable BSTree where
  -- foldr : (elem -> acc -> acc) -> acc -> BSTree elem -> acc
  foldr f = flip go where
    go : BSTree elem -> acc -> acc
    go  Empty       = id
    go (Node l v r) = go l . f v . go r

  -- foldl : (acc -> elem -> acc) -> acc -> PictureT elem -> acc
  foldl f = go where
    go : acc -> BSTree elem -> acc
    go a  Empty       = a
    go a (Node l v r) = go (f (go a l) v) r  

||| Inserts elements of a list into a binary search tree
listToTree : Ord a => List a -> BSTree a
listToTree = foldr insert Empty

||| Creates list from the elements of BSTree
treeToList : BSTree a -> List a
treeToList = foldr (::) Nil    -- Unsporting solution :)
