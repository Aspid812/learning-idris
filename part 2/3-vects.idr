import Data.Vect

-- Use implicit arguments to implement following function
vlength : Vect n a -> Nat
vlength {n} _ = n


-- sum of vectors (define recursively)
vadd : Num a => Vect n a -> Vect n a -> Vect n a
vadd  Nil     Nil    = Nil
vadd (x::xs) (y::ys) = (x + y) :: vadd xs ys


-- scalar product (use functions zipWith and sum)
vScalarProd : Num a => Vect n a -> Vect n a -> a
vScalarProd = curry (sum . uncurry (zipWith (*)))


-- replace all coordinates that are close to zero with zero
toAxis : (eps : Double) -> Vect n Double -> Vect n Double
toAxis eps = map (cling eps) where
  cling : (eps : Double) -> Double -> Double
  cling eps x = if (abs x < eps) then 0 else x


-- increase vector dimension, adding 0 as the first coordinate
incDimFirst : Vect n Double -> Vect (n+1) Double
incDimFirst xs = comm (0.0 :: xs) where
  comm : {n : Nat} -> Vect (1 + n) a -> Vect (n + 1) a
  comm {n = Z}    as     = as
  comm {n = S k} (a::as) = a :: comm as


-- increase vector dimension, adding 0 as the last coordinate
incDimLast : Vect n Double -> Vect (n+1) Double
incDimLast  Nil    = [0.0]
incDimLast (x::xs) = x :: incDimLast xs


-- project vector to the space given by vector of indices
-- use function `map`
project : Vect n a -> Vect k (Fin n) -> Vect k a
project v = map (flip index v)


test1 : Bool
test1 = project [1,2,3,4] [FS FZ, FZ, FS (FS FZ)] == [2,1,3]
        && project [1,2,3,4] [FS FZ, FS FZ, FS (FS (FS FZ))] == [2,2,4] 
        && project [0] [FZ, FZ, FZ, FZ] == [0,0,0,0]
--      Following tests don't compile, why? Reasons differ!
--      && project [1,2,3,4] [FS FZ, FS FZ, (FS (FS (FS (FS FZ))))] == [2,2,0] 
--      && project [0] [FZ, FZ, FZ, FZ] == [0,0,0]


-- reverse given vector
reverse' : Vect n a -> Vect n a
reverse'  Nil    = Nil
reverse' (x::xs) = append x (reverse' xs) where
  append : a -> Vect k a -> Vect (1 + k) a
  append x  Nil    = [x]
  append x (y::ys) = y :: append x ys


-- matrix transposition
transpose_mat : Vect m (Vect n elem) -> Vect n (Vect m elem)
transpose_mat  Nil      = replicate _ []
transpose_mat (xs::xss) = zipWith (::) xs (transpose xss)


-- matrix addition and multiplication
addMatrix : Num numType => Vect rows (Vect cols numType) 
                           -> Vect rows (Vect cols numType) 
                           -> Vect rows (Vect cols numType)
addMatrix = zipWith (zipWith (+))

multMatrix : Num numType => Vect n (Vect m numType) 
                            -> Vect m (Vect p numType) 
                            -> Vect n (Vect p numType)
multMatrix m1 m2 = [[vScalarProd u v | v <- transpose_mat m2] | u <- m1]
