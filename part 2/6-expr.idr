{-
An integer arithmetic expression can be one of the following:
  1. A single integer
  2. Addition of an expression to an expression
  3. Subtraction of an expression from an expression
  4. Multiplication of an expression with an expression
Define a recursive data type Expr which can be used to represent such
expressions.

Look at data type Picture (4-pictures.idr) for ideas.
-}

data Expr : Type where
  Value    : Int -> Expr
  Add      : Expr -> Expr -> Expr
  Subtract : Expr -> Expr -> Expr
  Multiply : Expr -> Expr -> Expr

-- Implement function, which evaluates an integer arithmetic expression.
evaluate : Expr -> Int 
evaluate (Value x)        = x
evaluate (Add ex ey)      = evaluate ex + evaluate ey
evaluate (Subtract ex ey) = evaluate ex - evaluate ey
evaluate (Multiply ex ey) = evaluate ex * evaluate ey
