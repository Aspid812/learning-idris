import Functions

{-
  Write a complete program which prompts for an input, calls 
  the function `counts` and prints its output.
-}



main : IO ()
main = do
	putStrLn "Words/chars counter. Please type a string:"
	str <- getLine
	let (numWords, numChars) = counts str
	putStrLn $ "There are " ++ show numWords ++ " words in " ++ show numChars ++ " characters"
