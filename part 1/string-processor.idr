import Functions

{-
  Write a complete repl-program which follows given protocol:
    * every input string starts with either P or C with one space after it
    * process rest of the string using `palindrome` or `counts` respectively
    
  Example session:
  > P noon
  noon is a palindrome
  > C Hello, Idris world!
  (3, 19)
  > P midday
  midday is not a palindrome
  
  Hints: 
    a) you may find functions `take`, `drop`, `words`, `unwords`, `substr`
       useful (make sure you understand what these functions do);
    b) try to implement as many auxiliary functions as you can.
-}

safe_str_head : String -> Maybe Char
safe_str_head str = if str /= ""
                    then Just (strHead str)
                    else Nothing

palindrome_cli : String -> String
palindrome_cli str = if palindrome str
                     then str ++ " is a palindrome\n"
                     else str ++ " is not a palindrome\n"

counts_cli : String -> String
counts_cli str = show (counts str) ++ "\n"

session : String -> String
session input = case safe_str_head input of
                  Nothing  => ""
                  Just 'P' => palindrome_cli $ substr 2 (length input) input
                  Just 'C' => counts_cli $ substr 2 (length input) input
                  _        => "Unrecognized command\n"

main : IO ()
main = repl "> " session
