import Functions

{-
  Write a complete program which prompts for an input, calls 
  the function `palindrome` and prints its output. Use `avemain.idr`
  from the second lecture as example.
-}



main : IO ()
main = do
	putStrLn "Palindrome checker. Please type a string:"
	str <- getLine
	if palindrome str
		then putStrLn "Yes, it's a palindrome!"
		else putStrLn "Not a palindrome"
