module Functions

%access export

{-
  1) Determine types of the following values without referring to REPL
  and then check your ideas with ":t". Don't forget to mention your 
  mistakes if any.
  
  1) ("A", "B", "C") : (String, String, String)      -- ok
  2) ["A", "B", "C"] : Vect 3 String                 -- mistake: List String
  3) ["A", "B", 'C'] : /ill-typed/                   -- ok
  4) ("A", "B", 'C') : (String, String, Char)        -- ok
  5) (("A", "B"), 'C') : ((String, String), Char)    -- ok
  6) ("A", ("B", 'C')) : (String, (String, Char))    -- mistake: (String, String, Char)
  7) [["A", "B"], ["C"]] : Vect 2 (List String)      -- mistake: List (List String)
  8) [("A", "B"), "C"] : /ill-typed/                 -- ok
-}

{-
  2) Implement case-insensitive function `palindrome` using 
  library functions reverse and toLower. Make sure your 
  implementation passes tests given.
-}

palindrome : String -> Bool
palindrome str = palindrome_case_sensitive (toLower str) where
  palindrome_case_sensitive : String -> Bool
  palindrome_case_sensitive str = str == reverse str


test_palindrome : String
test_palindrome =  if palindrome "pullup" 
                     && palindrome "Kayak"
                     && palindrome "noON"
                     && palindrome "Glenelg"
                     && palindrome "tattArratTat"
                     && palindrome "kuulilennuteetunneliluuk"
                     && not (palindrome "Idris")
                   then "Tests passed"
                   else "Tests failed"

-- Btw, do you know meanings of test words?

{-
  3) Write function `counts`, which returns a pair of the number of words 
  in the input and the number of characters in the input.  For example, 
  the input "Hello, Idris world!" should give the output (3, 19) .
  Provide tests for this function following the idea from previous exercise.
-}

counts : String -> (Nat, Nat)
counts str = (length (words str), length str)

test_counts : String
test_counts = if counts "Hello, Idris world!" == (3, 19) &&
                 counts "" == (0, 0) &&
                 counts "   " == (0, 3) &&
                 counts "Multiple  spaces   considered    as    a   single  separator" == (7, 60) &&
                 counts "   Leading and trailing spaces are OK   " == (6, 40) &&
                 counts "Sequences of @&^%$#! and numbers like 32768 are treated as regular  words" == (12, 73) &&
                 counts "U c@N m!x l3tt3r$ & n0n-L&77e?s" == (6, 31)
              then "counts: Test passed"
              else "counts: Test FAILED!"

{-
  4) Write function `top_ten`, which returns ten largest values in a list. 
  Hint: You may find functions `take` and `sort` useful (use :t and :doc for details). 
  Provide tests.
-}

top_ten: Ord a => List a -> List a
top_ten = List.take 10 . reverse . sort

test_top_ten : String
test_top_ten = if top_ten [5, 3, 6, 11, 65, 34, 48, 32, 7, 91, 53, 20, 78, 51, 34] == [91, 78, 65, 53, 51, 48, 34, 34, 32, 20] &&
                  top_ten [5, 8, 1, 4, 2, 6, 7, 10, 9, 3] == [10, 9, 8, 7, 6, 5, 4, 3, 2, 1] &&
                  top_ten [10, 9, 8, 7, 6, 5, 4, 3, 2, 1] == [10, 9, 8, 7, 6, 5, 4, 3, 2, 1] &&
                  top_ten [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] == [10, 9, 8, 7, 6, 5, 4, 3, 2, 1] &&
                  top_ten [4, 6, 1] == [6, 4, 1] &&
                  top_ten [4.0, 6.0, -1.0] == [6.0, 4.0, -1.0] &&
                  top_ten ["One", "Two", "Three"] == ["Two", "Three", "One"] &&
                  top_ten (the (List Int) []) == []
               then "top_ten: Test passed"
               else "top_ten: Test FAILED!"

{-
  5) Write function `over_length`, which returns the number of strings in the list
  longer than the given number of characters. For example, evaluating 
     over_length 3 ["One", "Two", "Three", "Four"] 
  should give the output 2.
  Provide tests.
-}

over_length : Nat -> List String -> Nat
over_length n = length . filter over_length_predicate where
  over_length_predicate : String -> Bool
  over_length_predicate str = length str > n

test_over_length : String
test_over_length = if over_length 3 ["One", "Two", "Three", "Four"] == 2 &&
                      over_length 0 ["One", "Two", "Three", "Four"] == 4 &&
                      over_length 5 ["One", "Two", "Three", "Four"] == 0 &&
                      over_length 3 [] == 0
               then "over_length: Test passed"
               else "over_length: Test FAILED!"
