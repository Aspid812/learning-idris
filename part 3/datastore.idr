module Main

import Data.Vect
import Control.Catchable

infixr 5 .+.

data Schema = SString                -- String component
            | SInt                   -- Int Component
            | (.+.) Schema Schema    -- Combination of schemas (use tuples)

SchemaType : Schema -> Type
-- Calculates type for the datastore elements
-- e.g. (String, String, Int) from (SString .+. (SString .+. SInt))
SchemaType  SString    = String
SchemaType  SInt       = Int
SchemaType (s1 .+. s2) = (SchemaType s1, SchemaType s2)

record DataStore where
  constructor MkData
  schema : Schema
  size : Nat
  items : Vect size (SchemaType schema)

addToStore : (store : DataStore) -> SchemaType (schema store) -> DataStore
addToStore (MkData sch n items) v = MkData sch (S n) new_items
where
  new_items = rewrite (plusCommutative 1 n) in items ++ [v]

setSchema : (store : DataStore) -> Schema -> Maybe DataStore
-- setting schema should be supported only if the store is empty, 
-- otherwise return Nothing
setSchema (MkData _ Z []) sch = Just (MkData sch Z [])
setSchema  _              _   = Nothing

-- User commands
data Command : Schema -> Type where
     SetSchema : Schema -> Command schema
     Add : SchemaType schema -> Command schema
     Get : Integer -> Command schema
     Quit : Command schema


namespace Util
  infixl 2 <$
  (<$) : Applicative f => a -> f b -> f a
  a <$ fb = pure a <* fb

  infixr 1 <=<
  (<=<) : Monad m => (b -> m c) -> (a -> m b) -> (a -> m c)
  g <=< f = \a => f a >>= g

  assert : Applicative f => Catchable f e => Bool -> e -> f ()
  assert p e = when (not p) (throw e)

  eitherToMaybe : Either e a -> Maybe a
  eitherToMaybe = either (const Nothing) Just

  filter : Alternative m => Foldable m => (a -> Bool) -> m a -> m a
  filter p = choiceMap (\a => a <$ guard (p a))

  strDecap : String -> Maybe (Char, String)
  strDecap s = case strM s of
    StrNil       => Nothing
    StrCons x xs => Just (x, xs)


namespace Parser
  ||| This function is just `liftA2` for my homemade implementation of `StateT`.
  seqWith : Monad m =>
    (a -> b -> c) ->      -- The function that combines actions' results
    (s1 -> m (a, s2)) ->  -- The first action
    (s2 -> m (b, s3)) ->  -- The second action
    (s1 -> m (c, s3))     -- A resulting composite action
  -- Feel the power of monad comphensions!
  seqWith f p q = \s1 => [ (f a b, s3) | (a, s2) <- p s1, (b, s3) <- q s2 ]

  ||| This function behaves like `liftA1` or `map`. It only applies a function
  ||| to the result of specified action, without affecting its essense.
  turnWith : Monad m =>   -- A `Functor` would be well enough... 
    (a -> b) ->           -- The function that maps the result of action
    (s1 -> m (a, s2)) ->  -- The action which result is to be turned
    (s1 -> m (b, s2))     -- A resulting action
  -- ...but there is no 'functor comprehensions' in Idris
  turnWith f p = \s1 => [ (f a, s2) | (a, s2) <- p s1 ]

  ||| An analog of inexistent `liftA0` or `pure` or `return`. The resulting
  ||| 'action' does actually performs no action but simply keeps a sole value.
  idleWith : Monad m =>   -- Like above: written `Monad`, intended `Applicative`
    a ->                  -- The value to be provided for descendants
    (s -> m (a, s))       -- A resulting dummy action
  -- Comprehension without a generator looks silly... And doesn't work at all 
  idleWith a = \s => pure (a, s)


  data Parser a = MkParser (String -> Maybe (a, String))

  implementation Functor Parser where
    map f (MkParser pa) = MkParser $ turnWith f pa

  implementation Applicative Parser where
    pure a = MkParser $ idleWith a
    (MkParser pf) <*> (MkParser pa) = MkParser $ seqWith id pf pa

  runParser : Parser a -> String -> Maybe (a, String)
  runParser (MkParser pa) = pa

  char : Char -> Parser Char
  char c = MkParser $ filter (\(x,_) => x == c) . strDecap

  span : (Char -> Bool) -> Parser String
  span f = MkParser $ pure . span f

  until : Char -> Parser String
  until c = span (/= c)

  spaces : Parser String
  spaces = span isSpace


parsePortion : (schema : Schema) -> String -> Maybe (SchemaType schema, String)
-- This function tries to parse one element of a schema (string or integer or pair)
-- in one recursive step, it is the harderst function to implement. 
-- Second component of the pair is what was left unparsed.
-- Use span, pack, ltrim, cast
parsePortion schema = runParser (parser_of schema) . ltrim where
  -- For convinient and by convention, `parser_of schema` expects no spaces before
  -- the relevant input and leaves no spaces after it
  parser_of : (schema : Schema) -> Parser (SchemaType schema)
  parser_of SInt        = cast <$> span isDigit <* spaces
  parser_of SString     = char '"' *> until '"' <* char '"' <* spaces
  parser_of (s1 .+. s2) = MkPair <$ char '(' <* spaces
                                 <*> parser_of s1 <* char ',' <* spaces
                                 <*> parser_of s2 <* char ')' <* spaces

  
parseBySchema : (schema : Schema) -> String -> Maybe (SchemaType schema)
parseBySchema schema x = case parsePortion schema x of
                              Nothing => Nothing
                              Just (res, "") => Just res -- returns Just only if
                                                         -- everything was parsed
                              Just _ => Nothing


parseSchema : String -> Maybe Schema
-- invent your own schema string representation and parse it
parseSchema = eitherToMaybe . (check_eof <=< do_parse) . unpack
where
  check_eof : (a, List Char) -> Either String a
  check_eof (r, xs) = r <$ assert (isNil xs) "Extra characters left after parsing"

  match : Char -> (a, List Char) -> Either String (a, List Char)
  match c (r, x::xs) = (r, xs) <$ assert (x == c) "Unexpected character"
  match _ (_, Nil)   = throw "Sudden termination of the stream"

  -- Straightforward grammar:  X ::= 'I' | 'S' | '(' X X ')'
  do_parse : List Char -> Either String (Schema, List Char)
  do_parse ('I'::xs) = pure (SInt, xs)
  do_parse ('S'::xs) = pure (SString, xs)
  do_parse ('('::xs) = seqWith (.+.) do_parse do_parse xs >>= match ')'
  do_parse (x::xs)   = throw ("Invalid type symbol: " ++ singleton x)
  do_parse  Nil      = throw "Sudden termination of the stream"


parseInteger : String -> Maybe Integer
parseInteger = map cast . filter (all isDigit . unpack) . filter (/= "") . pure


parse : (schema : Schema) -> (input : String) -> Maybe (Command schema)
-- invent your own syntax for user input and parse it
parse schema = parse_cmd . break isSpace . ltrim where
  parse_cmd : (String, String) -> Maybe (Command schema)
  parse_cmd ("schema", arg) = SetSchema <$> parseSchema (trim arg)
  parse_cmd ("add",    arg) = Add <$> parseBySchema schema arg
  parse_cmd ("get",    arg) = Get <$> parseInteger (trim arg)
  parse_cmd ("quit",   _)   = pure Quit
  parse_cmd (_,        _)   = throw ()


display : SchemaType schema -> String
-- pattern match over schema implicit argument to get information about types
-- and how to display them
display {schema=SInt}      val    = show val
display {schema=SString}   val    = "\"" ++ val ++ "\""
display {schema=(.+.) _ _} (x, y) = "(" ++ display x ++ ", " ++ display y ++ ")"


getEntry : (pos : Integer) -> (store : DataStore) -> Maybe (String, DataStore)
-- use integerToFin and index to extract data out of the store
getEntry pos store@(MkData _ n items) =
  [ (display (index k items), store) | k <- integerToFin pos n ]


-- Smells like `StateT DataStore Maybe String` spirit
processInput : DataStore -> String -> Maybe (String, DataStore)
processInput store input = case fromMaybe DoQuit (catch intention sorry) of
  DoUpdate new_store report => Just (report, new_store)
  DoInfo message            => Just (message, store)
  DoQuit                    => Nothing
where
  data Intention = DoQuit | DoInfo String | DoUpdate DataStore String

  sorry : () -> Maybe Intention
  sorry _ = pure $ DoInfo "Sorry, some shit seems to be happened...\n"

  intention : Maybe Intention
  intention = do
    cmd <- parse (schema store) input
    case cmd of
      SetSchema new_schema => do 
        new_store <- setSchema store new_schema
        pure $ DoUpdate new_store "Schema successfully changed\n"
      Add new_item => do
        let new_store = addToStore store new_item
        pure $ DoUpdate new_store "New entry added\n"
      Get index => do
        (entry, new_store) <- getEntry index store
        pure $ DoUpdate new_store (entry ++ "\n")
      Quit => do
        pure DoQuit


main : IO ()
main = replWith (MkData (SString .+. SString .+. SInt) _ []) "Command: " processInput
